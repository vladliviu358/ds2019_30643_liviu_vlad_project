﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models
{
    public class Medication
    {
        [Key]
        public int ID { get; set; }

        public String Name { get; set; }

        public String SideEffects { get; set; }

        public double Dosage { get; set; }

        [DataType(DataType.Time)]
        public DateTime TakeFrom { get; set; }

        [DataType(DataType.Time)]
        public DateTime TakeTo { get; set; }

        public bool Taken { get; set; }

        public ICollection<MedicationPacient> MedicationPacients { get; set; }
    }
}
