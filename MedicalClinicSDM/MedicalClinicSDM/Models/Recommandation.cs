﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models
{
    public class Recommandation
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public String Description { get; set; }
        [Required]
        public DateTime Timestamp{ get; set; }
        [Required]
        public int MedicID { get; set; }
        [Required]
        public Medic Medic { get; set; }
        [Required]
        public int PacientID { get; set; }
        [Required]
        public Pacient Pacient { get; set; }
    }
}
