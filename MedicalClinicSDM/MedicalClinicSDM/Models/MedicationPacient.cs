﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models
{
    public class MedicationPacient
    {
        public int MedicationID { get; set; }
        public Medication Medication { get; set; }

        public int PacientID { get; set; }
        public Pacient Pacient { get; set; }

    }
}
