﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using Microsoft.AspNetCore.Http;

namespace MedicalClinicSDM.Controllers
{
    public class MedicationPacientsController : Controller
    {
        private readonly MedicalClinicSDMContext _context;

        public MedicationPacientsController(MedicalClinicSDMContext context)
        {
            _context = context;
        }

        // GET: MedicationPacients
        public async Task<IActionResult> Index()
        {

            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var medicalClinicSDMContext = _context.MedicationPacient.Include(m => m.Medication).Include(m => m.Pacient);
            return View(await medicalClinicSDMContext.ToListAsync());
        }

        // GET: MedicationPacients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medicationPacient = await _context.MedicationPacient
                .Include(m => m.Medication)
                .Include(m => m.Pacient)
                .FirstOrDefaultAsync(m => m.MedicationID == id);
            if (medicationPacient == null)
            {
                return NotFound();
            }

            return View(medicationPacient);
        }

        // GET: MedicationPacients/Create
        public IActionResult Create()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            ViewData["MedicationID"] = new SelectList(_context.Medication, "ID", "Name");
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name");
            return View();
        }

        // POST: MedicationPacients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MedicationID,PacientID")] MedicationPacient medicationPacient)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (ModelState.IsValid)
            {
                _context.Add(medicationPacient);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MedicationID"] = new SelectList(_context.Medication, "ID", "Name", medicationPacient.MedicationID);
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name", medicationPacient.PacientID);
            return View(medicationPacient);
        }

        // GET: MedicationPacients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medicationPacient = await _context.MedicationPacient.FindAsync(id);
            if (medicationPacient == null)
            {
                return NotFound();
            }
            ViewData["MedicationID"] = new SelectList(_context.Medication, "ID", "Name", medicationPacient.MedicationID);
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name", medicationPacient.PacientID);
            return View(medicationPacient);
        }

        // POST: MedicationPacients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MedicationID,PacientID")] MedicationPacient medicationPacient)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id != medicationPacient.MedicationID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medicationPacient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicationPacientExists(medicationPacient.MedicationID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MedicationID"] = new SelectList(_context.Medication, "ID", "Name", medicationPacient.MedicationID);
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name", medicationPacient.PacientID);
            return View(medicationPacient);
        }

        // GET: MedicationPacients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medicationPacient = await _context.MedicationPacient
                .Include(m => m.Medication)
                .Include(m => m.Pacient)
                .FirstOrDefaultAsync(m => m.MedicationID == id);
            if (medicationPacient == null)
            {
                return NotFound();
            }

            return View(medicationPacient);
        }

        // POST: MedicationPacients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var medicationPacient = await _context.MedicationPacient.FindAsync(id);
            _context.MedicationPacient.Remove(medicationPacient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MedicationPacientExists(int id)
        {
            return _context.MedicationPacient.Any(e => e.MedicationID == id);
        }
    }
}
