﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using Microsoft.AspNetCore.Http;

namespace MedicalClinicSDM.Controllers
{
    public class CaregiverPacientsController : Controller
    {
        private readonly MedicalClinicSDMContext _context;

        public CaregiverPacientsController(MedicalClinicSDMContext context)
        {
            _context = context;
        }

        // GET: CaregiverPacients
        public async Task<IActionResult> Index()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var medicalClinicSDMContext = _context.CaregiverPacient.Include(c => c.Caregiver).Include(c => c.Pacient);
            return View(await medicalClinicSDMContext.ToListAsync());
        }

        // GET: CaregiverPacients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var caregiverPacient = await _context.CaregiverPacient
                .Include(c => c.Caregiver)
                .Include(c => c.Pacient)
                .FirstOrDefaultAsync(m => m.CaregiverID == id);
            if (caregiverPacient == null)
            {
                return NotFound();
            }

            return View(caregiverPacient);
        }

        // GET: CaregiverPacients/Create
        public IActionResult Create()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            ViewData["CaregiverID"] = new SelectList(_context.Caregiver, "ID", "Name");
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name");
            return View();
        }

        // POST: CaregiverPacients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CaregiverID,PacientID")] CaregiverPacient caregiverPacient)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (ModelState.IsValid)
            {
                _context.Add(caregiverPacient);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CaregiverID"] = new SelectList(_context.Caregiver, "ID", "Name", caregiverPacient.CaregiverID);
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name", caregiverPacient.PacientID);
            return View(caregiverPacient);
        }

        // GET: CaregiverPacients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var caregiverPacient = await _context.CaregiverPacient.FindAsync(id);
            if (caregiverPacient == null)
            {
                return NotFound();
            }
            ViewData["CaregiverID"] = new SelectList(_context.Caregiver, "ID", "Name", caregiverPacient.CaregiverID);
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name", caregiverPacient.PacientID);
            return View(caregiverPacient);
        }

        // POST: CaregiverPacients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CaregiverID,PacientID")] CaregiverPacient caregiverPacient)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id != caregiverPacient.CaregiverID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(caregiverPacient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CaregiverPacientExists(caregiverPacient.CaregiverID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CaregiverID"] = new SelectList(_context.Caregiver, "ID", "Name", caregiverPacient.CaregiverID);
            ViewData["PacientID"] = new SelectList(_context.Pacient, "ID", "Name", caregiverPacient.PacientID);
            return View(caregiverPacient);
        }

        // GET: CaregiverPacients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var caregiverPacient = await _context.CaregiverPacient
                .Include(c => c.Caregiver)
                .Include(c => c.Pacient)
                .FirstOrDefaultAsync(m => m.CaregiverID == id);
            if (caregiverPacient == null)
            {
                return NotFound();
            }

            return View(caregiverPacient);
        }

        // POST: CaregiverPacients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var caregiverPacient = await _context.CaregiverPacient.FindAsync(id);
            _context.CaregiverPacient.Remove(caregiverPacient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CaregiverPacientExists(int id)
        {
            return _context.CaregiverPacient.Any(e => e.CaregiverID == id);
        }
    }
}
