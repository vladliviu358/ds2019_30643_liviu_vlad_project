﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using Microsoft.AspNetCore.Http;

namespace MedicalClinicSDM.Controllers
{
    public class MedicationsController : Controller
    {
        private readonly MedicalClinicSDMContext _context;

        public MedicationsController(MedicalClinicSDMContext context)
        {
            _context = context;
        }

        // GET: Medications
        public async Task<IActionResult> Index()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");


            return View(await _context.Medication.ToListAsync());
        }

        public async Task<IActionResult> PacientIndex()
        {

            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");

            var consP = _context.MedicationPacient.Include(c => c.Pacient).Include(c => c.Medication).Where(c => c.PacientID == HttpContext.Session.GetInt32("UserID"));
            return View(await consP.ToListAsync());

        }


        // GET: Medications/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medication = await _context.Medication
                .FirstOrDefaultAsync(m => m.ID == id);
            if (medication == null)
            {
                return NotFound();
            }

            return View(medication);
        }

        // GET: Medications/Create
        public IActionResult Create()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            return View();
        }

        // POST: Medications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,SideEffects,Dosage,TakeFrom,TakeTo")] Medication medication)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (ModelState.IsValid)
            {
                _context.Add(medication);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(medication);
        }

        // GET: Medications/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medication = await _context.Medication.FindAsync(id);
            if (medication == null)
            {
                return NotFound();
            }
            return View(medication);
        }

        // POST: Medications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,SideEffects,Dosage,TakeFrom,TakeTo")] Medication medication)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id != medication.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medication);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicationExists(medication.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(medication);
        }

        // GET: Medications/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medication = await _context.Medication
                .FirstOrDefaultAsync(m => m.ID == id);
            if (medication == null)
            {
                return NotFound();
            }

            return View(medication);
        }

        // POST: Medications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var medication = await _context.Medication.FindAsync(id);
            _context.Medication.Remove(medication);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MedicationExists(int id)
        {
            return _context.Medication.Any(e => e.ID == id);
        }
    }
}
