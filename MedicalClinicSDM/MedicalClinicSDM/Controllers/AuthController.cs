﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using MedicalClinicSDM.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static MedicalClinicSDM.Models.ViewModels.LoginViewModel;

namespace MedicalClinicSDM.Controllers
{
    public class AuthController : Controller
    {
        private readonly MedicalClinicSDMContext _context;

        public AuthController(MedicalClinicSDMContext context)
        {
            _context = context;
        }

        public IActionResult Login()
        {
            ViewBag.TipCont = "0";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Username, Password")] LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                
                var pacients = _context.Pacient.ToList();
                var medics = _context.Medic.ToList();
                var caregivers = _context.Caregiver.ToList();

                foreach (Pacient p in pacients)
                {
                    if (p.Username.Equals(login.Username) && p.Password.Equals(login.Password) && p.AccountType.Equals(1))
                    {
                        HttpContext.Session.SetInt32("UserID", p.ID);
                        HttpContext.Session.SetString("User", p.Username);
                        HttpContext.Session.SetString("TipCont", p.AccountType.ToString());
                        return RedirectToAction("Details", "Pacients", new { id = HttpContext.Session.GetInt32("UserID") });

                    }
                    else
                    {
                        ModelState.AddModelError("Error", "");
                    }
                }

                foreach (Medic m in medics)
                {
                    if (m.Username.Equals(login.Username) && m.Password.Equals(login.Password) && m.AccountType.Equals(2))
                    {
                        HttpContext.Session.SetInt32("UserID", m.ID);
                        HttpContext.Session.SetString("User", m.Username);
                        HttpContext.Session.SetString("TipCont", m.AccountType.ToString());
                        return RedirectToAction("Index", "Medics");
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "");
                    }
                }

                foreach (Caregiver a in caregivers)
                {
                    if (a.Username.Equals(login.Username) && a.Password.Equals(login.Password) && a.AccountType.Equals(3))
                    {

                        HttpContext.Session.SetInt32("UserID", a.ID);
                        HttpContext.Session.SetString("User", a.Username);
                        HttpContext.Session.SetString("TipCont", a.AccountType.ToString());

                        return RedirectToAction("Details", "Caregivers", new { id = HttpContext.Session.GetInt32("UserID") });
                    }
                    else
                    {
                        ViewBag.TipCont = "0";
                        ModelState.AddModelError("Error", "");
                    }
                }
            }
            else

            {
                ViewBag.TipCont = "0";
                ModelState.AddModelError("Error", "");
            }
            return View();
        }

        public IActionResult Register()
        {
            ViewBag.TipCont = "0";
            return View();
        }


    }
}      